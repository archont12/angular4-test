# Angular 4 Lazy Loading Boilerplate

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Install Angular 4 CLI

Run `npm install -g @angular/cli`

## Development server

Run `npm install`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

Intelex Mobile V2 repo

See the document SetupLocalMachineMobileV2 at this SMB location for steps on
how to setup your Mac to work with MobileV2

\\winston\Departments\All_Departments\R&D\Onboarding\Documents\Developer

For Automation, see the README.md in UIAutomation/calabash

## Git Hooks

To enable git hooks run `rake setup_git_hooks` in rood directory.

## Flow

Run `npm install` to install all required plugins.

Run `npm run flow` to initialize type checker.

[Flow guidelines](./FLOWGUIDELINES.md)

[Flow docs](https://flow.org/en/docs/)

## What should be covered

*   [Functions](https://flow.org/en/docs/types/functions/)
*   [Classes](https://flow.org/en/docs/types/classes/)

> **Note:** please create [type aliases](https://flow.org/en/docs/types/aliases/) for complex data types

