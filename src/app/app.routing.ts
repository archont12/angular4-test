import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './users/view/view.component';
import { ListComponent } from './users/list/list.component';
import { EditComponent } from './users/edit/edit.component';

import { ListResolve } from './users/list/list.resolve';

const routes: Routes = [{
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }, {
    path: 'list',
    component: ListComponent,
    resolve: {
      users: ListResolve
    }
  }, {
    path: 'view/:id',
    component: ViewComponent
  }, {
    path: 'edit/:id',
    component: EditComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
