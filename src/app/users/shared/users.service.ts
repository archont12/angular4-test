import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { APP_CONFIG, AppConfig } from '../../app-config.module';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UsersService {

  constructor(
    private http: Http,
    @Inject(APP_CONFIG) private config: AppConfig
  ) { }

  private usersUrl = '/users';

  public getUsers(): Observable<Response> {
    return this.http
       .get(`${this.config.apiEndpoint}${this.usersUrl}`)
       .map(res => res.json());
  }

  public getUserById(id: String): Observable<Response> {
    return this.http
       .get(`${this.config.apiEndpoint}/${this.usersUrl}/${id}`);
  }
}
