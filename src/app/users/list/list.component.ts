import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { path } from 'ramda';

@Component({
  selector: 'users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  private users = [];

  constructor(private route: ActivatedRoute) {
    this.users = path(['snapshot', 'data', 'users'], route) || [];
  }

  ngOnInit() { }
}
