import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { UsersService } from '../shared/users.service';

export interface Users { }

@Injectable()
export class ListResolve implements Resolve<Users> {

  constructor(private usersService: UsersService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.usersService.getUsers();
  }
}
