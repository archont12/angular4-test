import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppConfigModule } from './app-config.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { NavbarComponent } from './common/navbar/navbar.component';
import { HomeComponent } from './common/home/home.component';
import { FooterComponent } from './common/footer/footer.component';
import { ListComponent } from './users/list/list.component';
import { EditComponent } from './users/edit/edit.component';
import { ViewComponent } from './users/view/view.component';

import { UsersService } from './users/shared/users.service';
import { ListResolve } from './users/list/list.resolve';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    ListComponent,
    EditComponent,
    ViewComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    AppConfigModule
  ],
  providers: [
    UsersService,
    ListResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
